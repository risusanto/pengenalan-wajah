import cv2
import numpy as numpy
import os, time
import dlib
from imutils import face_utils
from imutils.face_utils import FaceAligner
import time

detector = dlib.get_frontal_face_detector()
FACE_DIR = "train_data/images/"

def create_folder(folder_name):
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)


def main():
    create_folder(FACE_DIR)
    name=input("Enter Name: ")
    total=input("Num Samples: ")

    
    cap = cv2.VideoCapture(0)
    total_imgs = int(total)
    img_no = 0
    while True:
        ret, img = cap.read()
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = detector(img_gray)
        if len(faces) == 1:
            face = faces[0]
            (x, y, w, h) = face_utils.rect_to_bb(face)
            face_img = img[y:y + h, x:x + w]
            face_img = cv2.resize(face_img,(96,96))

            img_dir = os.path.join(FACE_DIR,name)
            create_folder(img_dir)
            filename = str(img_no) + '.jpg'
            img_path = os.path.join(img_dir,filename)
            cv2.imwrite(img_path, face_img)

            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 255, 0), 3)
            img_no += 1
            cv2.imshow("Saving " + str(img_no), img)
            cv2.waitKey(3)
            time.sleep(3)

        
        # cv2.waitKey(1)
        if img_no == total_imgs:
            break

    cap.release()


main()
