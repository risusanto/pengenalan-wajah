from keras.applications.vgg16 import VGG16
import split_folders
from keras import layers, models
from keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array
import numpy as np
from keras.callbacks import CSVLogger, ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from keras.optimizers import SGD
from keras.regularizers import l2
import simple_cnn

import argparse
import pickle
import os
from pyprind import  ProgBar

def create_label_encoder():
    images_path = 'train_data/images'
    cls_name = []
    for i in sorted(os.listdir(images_path)):
        cls_name.append(i)

    pickle.dump(cls_name, open('face_ai/models/label_encoder', 'wb'))

    return cls_name


def create_model_vgg16(num_classes):
    conv_base = VGG16(weights='imagenet',
                      include_top=False,
                      input_shape=(96, 96, 3))

    conv_base.trainable = False

    model = models.Sequential()
    model.add(conv_base)
    model.add(layers.Flatten())
    model.add(layers.Dense(1024, activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(num_classes, kernel_regularizer=l2(.0005), activation='softmax'))

    return model

def create_simple_model(num_classes):
    return simple_cnn.build((96,96,3),num_classes)


def create_data_generator(train_dir, test_dir, batch_size):

    data_gen = ImageDataGenerator(rotation_range=0,
                               featurewise_center=False,
                               featurewise_std_normalization=False,
                               horizontal_flip=False,
                               vertical_flip=False,
                               data_format='channels_last',
                               rescale=1./255
                               )

    train_data = data_gen.flow_from_directory(
        directory=train_dir,
        target_size=(96, 96),
        color_mode='rgb',
        batch_size=batch_size,
        seed=11
    )
    validation_data = data_gen.flow_from_directory(
        directory=test_dir,
        target_size=(96, 96),
        color_mode='rgb',
        batch_size=batch_size,
        seed=11
    )

    return train_data, validation_data


def train_model(model, epochs, batch_size):
    create_label_encoder()
    split_folders.ratio('train_data/images', output="train_data/dataset", 
                        seed=1337, ratio=(.8, .2))

    train_data, validation_data = create_data_generator('train_data/dataset/train',
                                                        'train_data/dataset/val',
                                                        batch_size)

    # callbacks
    base_path = 'train_data/'
    patience = 50
    log_file_path = base_path + 'logs/training.log'
    csv_logger = CSVLogger(log_file_path, append=False)
    reduce_lr = ReduceLROnPlateau('val_loss', factor=0.1,
                                  patience=int(patience / 4), verbose=1)
    early_stop = EarlyStopping('val_loss', patience=patience)
    trained_models_path = base_path + 'models/_models_'
    model_names = trained_models_path + '.{epoch:02d}-{val_acc:.2f}.hdf5'
    model_checkpoint = ModelCheckpoint(model_names, 'val_loss', verbose=1, save_best_only=True)
    callbacks = [model_checkpoint, csv_logger, reduce_lr, early_stop]

    opt = SGD(lr=.01, momentum=.9)
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])

    model.fit_generator( generator=train_data, validation_data=validation_data,
        steps_per_epoch= 128,
        validation_steps= 128,
        epochs=epochs,
        verbose=1,
        callbacks=callbacks
    )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train face recognition model')
    parser.add_argument('--numclasses', '-n', type=int, default=11,
                        help='Number of classes')
    parser.add_argument('--batchsize', '-b', type=int, default=32,
                        help='Number of images in each mini-batch')
    parser.add_argument('--epochs', '-e', type=int, default=1000,
                        help='Number of sweeps over the dataset to train')
    parser.add_argument('--model', '-m', default='vgg16',
                        help='model selection')
    parser.add_argument('--option', '-o', default='start_train',
                        help='Option for models')
    args = parser.parse_args()

    if args.model == 'vgg16':
        model = create_model_vgg16(args.numclasses)
    else:
        model = model = create_simple_model(args.numclasses)

    if args.option == 'model_details':
        model.summary()
    elif args.option == 'start_train':
        train_model(model, args.epochs, args.batchsize)
    else:
        print(f'Error: No option name: {args.option}')
