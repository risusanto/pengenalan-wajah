from keras.models import load_model
import keras.backend
import tensorflow as tf

import numpy as np
import pickle
import time

# initializing the graph
graph = tf.get_default_graph()

_backend = keras.backend.backend()

# loading our trained model
print("Model loading.......")
model = load_model('face_ai/models/face_ai.hdf5', compile=False)
label_encoder = pickle.load(open('face_ai/models/label_encoder', 'rb'))
print("Model loaded!!")


def predict(img):
    start_time = time.time()

    if _backend == 'tensorflow':
        with graph.as_default():
            prediction = model.predict(np.expand_dims(img, axis=0))[0]
    else:
        prediction = model.predict(np.expand_dims(img, axis=0))[0]

    respond_time = time.time() - start_time
    label = label_encoder[np.argmax(prediction)]

    return label, prediction, respond_time


def predict_batch(x):
    start_time = time.time()

    for i in range(0, len(x)):
        x[i] = (x[i] / 225.)

    if _backend == 'tensorflow':
        with graph.as_default():
            predictions = model.predict(x)
    else:
        predictions = model.predict(x)

    labels = []
    for pred in predictions:
        label = label_encoder[np.argmax(pred)]
        labels.append(label)

    respond_time = time.time() - start_time

    return labels, predictions, respond_time
