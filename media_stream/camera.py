import cv2
import numpy as np
import dlib
from imutils import face_utils
from face_ai import utils
import time


def recognize(face, image, id):
    start_time = time.time()
    (fX, fY, fW, fH) = face_utils.rect_to_bb(face)
    roi = image[fY:fY + fH, fX:fX + fW]
    roi = cv2.resize(roi, (96, 96))
    label, prediction, respond_time = utils.predict(roi)
    respond_time = time.time() - start_time

    acc = prediction[np.argmax(prediction)] * 100

    prob = "%.2f" % acc
    respond_time = "%.2f" % respond_time

    time_format = 'Respond Time: {0}s'.format(respond_time)
    opt = '{0}. {1}'.format(id,label)

    if acc > 98 :
        cv2.putText(image, opt, (fX, fY - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)

    cv2.rectangle(image, (fX, fY), (fX + fW, fY + fH),
                  (244, 212, 66), 1)

    cv2.putText(image, time_format, (0, 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)
    return image


class VideoCamera(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)
        self.detector = dlib.get_frontal_face_detector()

    def __del__(self):
        self.video.release()

    def get_frame(self):
        success, image = self.video.read()
        image = cv2.flip(image, 1)
        #image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)

        faces = self.detector(image)

        if len(faces) > 0:
            i = 1
            for face in faces:
                try:
                    image = recognize(face=face, image=image,id=i)
                except:
                    print('error')
                i += 1

        ret, jpeg = cv2.imencode('.jpg', image)

        return jpeg.tobytes()
